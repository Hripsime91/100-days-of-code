from game_data import data
import random

account_b = random.choice(data)
score = 0
game_should_continue = True
while game_should_continue:
    # Generate a random account from the game_data.
    account_a = account_b
    account_b = random.choice(data)

    def formate_account(account):
        """Format the account data into printable formate."""
        account_name = account["name"]
        account_descr = account["description"]
        account_country = account["country"]
        return f"{account_name}, a {account_descr} from {account_country}"

    def check_answer(guess, a_followers, b_followers):
        """Use if statement to check if user is correct"""
        if a_followers > b_followers:
            return guess == "a"
        else:
            return guess == "b"

    while account_a == account_b:
        account_b = random.choice(data)
    print(f"Compare A: {formate_account(account_a)}.")
    print("VS")
    print(f"Against B: {formate_account(account_b)}.")
    guess = input("Hwo has more followers? Type 'A' or 'B': ").lower()

    a_follower_count = account_a["follower_count"]
    b_follower_count = account_b["follower_count"]
    is_correct = check_answer(guess, a_follower_count, b_follower_count)
    if is_correct:
        score += 1
        print(f"You are right! Current score {score}")
    else:
        game_should_continue = False
        print(f"Sorry, that's wrong. Final score {score}")
